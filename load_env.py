from dotenv import load_dotenv
import os

# Specify the path to your .env file
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

# Load the environment variables
load_dotenv(dotenv_path)
print(dotenv_path)