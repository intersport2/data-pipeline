import snowflake.connector
import pandas as pd
import os
import sys
import json
from dotenv import load_dotenv
import supplement_functions as supp

# Get the environment
if len(sys.argv) > 1:
    environment = sys.argv[1]
else:
    environment = 'dev'  # Default value

load_dotenv() 

df = ''
 
# accessing and printing value
print(os.getenv("SNOWFLAKE_USERNAME"))
sf_user = os.getenv("SNOWFLAKE_USERNAME")
sf_password = os.getenv("SNOWFLAKE_PASSWORD")
sf_account = os.getenv("SNOWFLAKE_ACCOUNT")
sf_warehouse = os.getenv("SNOWFLAKE_WAREHOUSE")
sf_role = os.getenv("SNOWFLAKE_ROLE")
sf_database = os.getenv("SNOWFLAKE_DATABASE")
sf_schema = os.getenv("SNOWFLAKE_SCHEMA")

data_files = {
    "customer.csv": "customer",
    "order.csv": "order",
    "product.csv": "product"
}

current_directory = os.getcwd()
config_folder = os.path.join(current_directory,'config')

sf_stage = ''

with snowflake.connector.connect(
    user=sf_user,
    password=sf_password,
    account=sf_account,
    warehouse=sf_warehouse,
    role=sf_role,
    database=sf_database,
    schema=sf_schema
) as sf_conn:
    with sf_conn.cursor() as sf_cursor:
        for file, table in data_files.items():
            file_path = os.path.join('data', file)
            df = pd.read_csv(f'{file_path}')
            config_file_path = os.path.join(config_folder, f'{table}.json')
            with open(config_file_path, "r") as _file:
                config_file = json.loads(_file.read())
            columns = ','.join([attr["name"] + ' ' + attr["type"] for attr in config_file['attributes']])
            
            column_headers = list(df.columns)
            print(column_headers)
            json_data = df.to_json(orient='records')
            json_records = json.loads(json_data.replace("'", "''"))
            batch_size = 5000  # Number of records per batch
            batches = [json_records[i:i+batch_size] for i in range(0, len(json_records), batch_size)]

            try:
                for batch in batches:
                    batch_insert_sql = f"""
                        INSERT INTO {sf_schema}.stage_{table} (raw_json_data)
                        SELECT PARSE_JSON(column1)
                        FROM VALUES {','.join([f"('{json.dumps(record)}')" for record in batch])}
                    """
                sf_cursor.execute(f"""
                        CREATE TABLE IF NOT EXISTS {sf_schema}.raw_{table} (
                            id BIGINT IDENTITY(1,1),
                            {columns},
                            is_processed BOOLEAN DEFAULT FALSE,
                            added_on TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP()
                        )
                    """)
                merge_sql = supp.create_merge_statement(f'{sf_schema}.raw_{table}', f'{sf_schema}.stage_{table}', config_file)
                print(merge_sql)
                sf_cursor.execute(merge_sql)
            except Exception as e:
                print(f'An unexpected error occurred: {e}')

