# Data Pipeline

# Case Study information
1. load the 3 files into a database of your choice.


Files are being loaded through python file load_initial_data.py
The data is being loaded into snowflake db. Currently, the process converts the data into a json format and loads into a temporary stage table. A merge statement is framed dynamically through the profile files of each table present in config folder. In case of a real project, I would have uploaded csv files to S3 bucket and used S3 as an external stage to load the data in snowflake.

To load the data we have to run the python file with the environment name as a sys argument. For dev environment, the command would look like below:

python load_initial_data.py dev

2. create a DWH "mid layer" data model  for the data.

I am following a medallion architecture here where the raw data is stored in tables present in BRONZE schema.
The mid layer is the SILVER schema where data is cleansed, validated and also the SCD types are defined.Below is the data model for SILVER schema:
![alt text](image.png)

The tables PRODUCT_DATA, CUSTOMER_DATA and EXCHANGE_RATES are managed as SCD Type 2. For ease, I have considered all attributes as historic attributes, which means that whenever any attribute other than the primary key changes, a history of that row is maintained.
Every other table has an ADDED_ON and UPDATED_ON column to keep track of when the data got updated and are basically SCD Type 1
Snowflake does not have indexes. The data is already stored in micro-partitions and is highly performant. Snowflake does have cluster keys which can be used to tell snowflake how the micro-partitions are made. From the data, I could say only the order_details table might use a combination of ORDER_ID and ORDER_DATE as a cluster key.

3. Load the data into mid layer tables
The data for dimension tables EXCHANGE_RATES, PRODUCT_VERTICAL and PRODUCT_SUB_VERTICAL is done through an insert operation (present in the create scripts of the tables respectively in the folder ddls/tables)
The data for other tables is inserted through stored procedure calls. The procedure definition can be found in the folder ddls/procedures


CALL INTERSPORT_DEV.SILVER.LOAD_CUSTOMER_DATA_SP (0)

CALL INTERSPORT_DEV.SILVER.LOAD_PRODUCT_DATA_SP (0)

CALL INTERSPORT_DEV.SILVER.LOAD_ORDER_DETAILS_SP (0)

For ease, I have not included the logging or alerting logic in the procedures. Also, these procedures can be called on a schedule using tasks in snowflake or airflow dags. My initial plan was to use dbt models however dbt does not allow the use of IDENTITY columns (there might be a workaround for that but due to time limitations I did not research more on this). I am using IDENTITY columns to behave as surrogate keys for my SCD Type 2 tables.

4. How to ensure high data quality in the "mid layer" tables and stability of the load process.


Each source table has IS_PROCESSED flag which indicates the rows which have to be processed in the next run. This helps in processing only the new records. Additionally, we could use STREAMS to properly capture CDC in Snowflake. 
We can add a de-duplication logic to avaoid inserting duplicate records and use NOT NULL constraints on specific columns to avoid accidental insert of null values.
All sql statements in the load process would be queried in one single transaction, so any error would cause a rollback

5. Tables for the 3rd layer (Gold schema)


The tables for 3rd layer are created in the GOLD schema. The ddls of these tables can be found in the folder ddls/tables/gold/ alongwith the query for initial insert.

The tables are:

GOLD.CUSTOMER_PURCHASES (This contains aggregate data of all purchases made by the customers including the total purchase amount in their currency and also in EUR) Note- depending on the order status, the purchase amount can be the actual purchase amount or the total amount refunded etc.
Data can be loaded incrementally by calling below procedure (ddls/procedures/gold/load_customer_purchases.sql)

CALL INTERSPORT_DEV.GOLD.LOAD_CUSTOMER_PURCHASES_SP (0)

GOLD.PRODUCT_PURCHASES (This contains aggregate data of all products purchased in specific date)
Data can be loaded incrementally by calling below procedure (ddls/procedures/gold/load_Product_purchases.sql)

CALL INTERSPORT_DEV.GOLD.LOAD_PRODUCT_PURCHASES_SP (0)

GOLD.CUSTOMER_PRODUCT_PURCHASES (This contains information about purchases made by customer for each date)
Data can be loaded incrementally by calling below procedure (ddls/procedures/gold/load_customer_product_purchases.sql)

CALL INTERSPORT_DEV.GOLD.LOAD_CUSTOMER_PRODUCT_PURCHASES_SP (0)

All these procedures can be scheduled to call from snowflake tasks or airflow dags. In airflow dags, we can also make the use of sensors to check if the previous dependant tables have been filled before running these loads. Also, logging and alerting logic should be placed.

# CRM analysis
The tables in gold layer can be used for different analyses like identifying monthly sales trend, customers who bought a specific brand in last 2 years etc. All queries for analyses are placed in the folder sample_queries
