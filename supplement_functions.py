
def create_merge_statement(target_table: str, source_table: str, profile: dict):
    """
	Executes the merge statement.
	The merge statement is build based on the passed profile.
	"""
    source_alias = 'SOURCE'
    target_alias = 'TARGET'
    attributes = [[attr["name"], attr["type"]] for attr in profile["attributes"]]
    transformations = profile["merge"]["transformation"]
    attributes_select = ',\n'.join(
        [ 'RAW_JSON_DATA:' + transformations[attr[1]].replace('{value}', attr[0]) + ' as ' + attr[0] for attr in attributes])

    join_criteria = ' AND \n'.join(
        [target_alias + '.' + attr["name"] + ' = ' + source_alias + '.' + attr["name"] for
         attr in
         profile["merge"]["primary_key"]])

    attributes_update = ',\n'.join(
        [target_alias + '.' + attr["name"] + ' = ' + source_alias + '.' + attr["name"] for
         attr in profile["attributes"] if "update" in attr["operation"]])

    update_condition = ' OR \n'.join(
        [target_alias + '.' + attr["name"] + ' <> ' + source_alias + '.' + attr["name"] for
         attr in profile["attributes"] if "update" in attr["operation"]])
    update_condition = '(' + update_condition + ')'

    attributes_insert_cols = ','.join(
        [attr["name"] for attr in profile["attributes"]])
    attributes_insert_vals = ','.join(
        [source_alias + '.' + attr["name"] for attr in profile["attributes"]])    

    _stmt = f'merge into {target_table} AS {target_alias} using (\n'
    _stmt += f'select {attributes_select}\n'
    _stmt += f'from {source_table}\n'
    _stmt += f') as {source_alias}\n'
    _stmt += f'on {join_criteria}\n '
    _stmt += f'when matched and {update_condition} then update set\n'
    _stmt += f'{attributes_update}\n'
    _stmt += 'when not matched then insert (\n'
    _stmt += f'{attributes_insert_cols}\n'
    _stmt += ') values (\n'
    _stmt += f'{attributes_insert_vals}\n'
    _stmt += ')'

    return _stmt