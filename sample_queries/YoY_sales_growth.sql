--Identify year on year sales growth by product brand

;with cte_yearly_sales as 
(select BRAND, SUM(TOTAL_SALES_AMOUNT_IN_EUR) AS YEARLY_SALES,
YEAR(PURCHASE_DATE) as YEAR from gold.product_purchases where order_status = 'delivered'
group by BRAND, YEAR(PURCHASE_DATE)),
cte_yearly_sales_including_prev_year as
(select BRAND, YEAR, 
LAG(YEARLY_SALES) OVER (partition by brand order by year) as PREV_YEAR_SALES,
YEARLY_SALES as CURRENT_YEAR_SALES from cte_yearly_Sales)
select BRAND, YEAR, 
CASE WHEN PREV_YEAR_SALES IS NOT NULL THEN
ROUND(((CURRENT_YEAR_SALES - PREV_YEAR_SALES) / PREV_YEAR_SALES) * 100, 2)
ELSE NULL END as YOY_SALES_GROWTH