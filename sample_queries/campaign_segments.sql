-- Get all customers who bought the brand Nike in the last 24 months
SELECT 
    CONCAT(cpp.BRAND,' - Campaign') AS CAMPAIGN_NAME,
    c.CUSTOMER_ID,
    CONCAT(c.first_name, ' ', c.last_name) as CUSTOMER_NAME
FROM 
    GOLD.CUSTOMER_PRODUCT_PURCHASES cpp
JOIN 
    GOLD.CUSTOMER_PURCHASES c
ON 
    cpp.CUSTOMER_ID_SK = c.ID
WHERE 
    cpp.PURCHASE_DATE >= DATEADD(MONTH, -24, CURRENT_DATE())
    and cpp.BRAND = 'Nike'