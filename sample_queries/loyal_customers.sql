--Identify loyal customers based on criteria if they made more than 10 purchases overall 
--and their last purchase was within the last 2 month

select CUSTOMER_ID,
CONCAT(FIRST_NAME, ' ', LAST_NAME) AS CUSTOMER_NAME,
EMAIL_ADDRESS, total_purchases,
SUM(TOTAL_PURCHASES) OVER (PARTITION BY CUSTOMER_ID, CUSTOMER_NAME, EMAIL_ADDRESS) AS PURCHASE_COUNT
from gold.customer_purchases
where LAST_PURCHASE_DATE >= DATEADD(MONTH, -2, CURRENT_DATE())
QUALIFY PURCHASE_COUNT >= 10;