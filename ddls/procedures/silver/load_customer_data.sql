
create or replace procedure INTERSPORT_DEV.SILVER.LOAD_CUSTOMER_DATA_SP (
	AUTO_COMMIT boolean
)
returns variant
language javascript
execute as caller
as
$$
	
	var status;
	var result = {
		"status":"",
		"time_begin":"",
		"time_end":"",
		"messages":""
	};
	var message;
	var messageArray = [];
	
	// variables: sql execution
	var sql_session_id = " select current_session(); "
	var stmt;
	var stmt_result;
	var next_stmt;

	var time_begin_main;
	var time_end_main;
	var time_begin_sub;
	var time_end_sub;
	
	var sql_cmd_set_session_tag = "alter session set query_tag = 'LOAD_CUSTOMER_DATA';";
	var procedure_name = "SILVER.LOAD_CUSTOMER_DATA_SP";
	var error_message;
	var sql_send_sns;
	
	// sql table names
	var source_table												= "BRONZE.RAW_CUSTOMER"
	var target_table												= "SILVER.CUSTOMER_DATA"
	var temp_customer_insert_stage_table							= "CUSTOMER_STAGE_INSERT_TBL";
	var temp_customer_update_stage_table 							= "CUSTOMER_STAGE_UPDATE_TBL";

	// variables: sql statements
	
	/*
	 Create temporary staging table for storing all unprocessed customer data
	*/
	var sql_create_temp_customer_insert_stage_table = `
				create or replace temporary table ${temp_customer_insert_stage_table} as
				SELECT *
				FROM ${source_table} WHERE is_processed = FALSE 
				; 
	`;

	/*
	 Create temporary staging table for customer data which has to be updated
	*/
	var sql_create_temp_customer_update_stage_table = `
				create or replace temporary table ${temp_customer_update_stage_table} as
				SELECT *
				FROM ${source_table} WHERE is_processed = FALSE AND
				CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM ${target_table})
				; 
	`;
			
	/*
	 Update effective_till date for SCD type 2
	*/
	var sql_update_historic_data = `
				update ${target_table} 
				SET EFFECTIVE_TILL = CURRENT_TIMESTAMP()
				WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM ${temp_customer_update_stage_table})
				AND EFFECTIVE_TILL IS NULL				
				;			
	`;

	/*
	 Insert the data into customer_data
	*/
	var sql_cmd_insert_customer_data = `
				insert into ${target_table} 
				(CUSTOMER_ID,
				FIRST_NAME,
				LAST_NAME,
				GENDER,
				BIRTH_DATE,
				EMAIL_ADDRESS,
				COUNTRY,
				ZIP_CODE,
				STREET,
				STREET_NUMBER,
				EFFECTIVE_FROM)
				SELECT CUSTOMER_ID,
				SPLIT_PART(CUSTOMER_NAME, ' ', 1) AS FIRST_NAME,
				SPLIT_PART(CUSTOMER_NAME, ' ', 2) AS LAST_NAME,
				GENDER,
				BIRTH_DATE,
				EMAIL_ADDRESS,
				COUNTRY,
				ZIP_CODE,
				STREET,
				STREET_NUMBER,
				CURRENT_TIMESTAMP() AS EFFECTIVE_FROM
				from
				 ${temp_customer_insert_stage_table}				
				;			
	`;

	/*
	 Update IS_PROCESSED flag in raw table
	*/
	var sql_cmd_update_flag_raw_table = `
				UPDATE ${source_table} SET IS_PROCESSED = TRUE 
				WHERE ID IN (SELECT ID FROM ${temp_customer_insert_stage_table})				
				;			
	`;

	/*
	 Drop insert stage table
	*/
	var sql_cmd_drop_insert_stage_table = `
				DROP TABLE ${temp_customer_insert_stage_table}				
				;			
	`;

	/*
	 Update update stage table
	*/
	var sql_cmd_drop_update_stage_table = `
				DROP TABLE ${temp_customer_update_stage_table}				
				;			
	`;
	

	// list of sql commands to be executed and corresponding log message
	var executions_array = [];
	executions_array.push(
		[sql_create_temp_customer_insert_stage_table, 'create temporary stage table '+temp_customer_insert_stage_table],
		[sql_create_temp_customer_update_stage_table, 'create temporary stage table '+temp_customer_update_stage_table],
		[sql_update_historic_data, 'update historic data in '+target_table],
		[sql_cmd_insert_customer_data, 'insert new records in '+target_table],
		[sql_cmd_update_flag_raw_table, 'update IS_PROCESSED flag in '+source_table],
		[sql_cmd_drop_insert_stage_table, 'drop stage table '+temp_customer_insert_stage_table],
		[sql_cmd_drop_update_stage_table, 'drop stage table '+temp_customer_update_stage_table]
		)

	try {
		time_begin_main = new Date().toISOString();
		result["time_begin"] = time_begin_main;
		
		// 00: set session tag
		next_stmt = sql_cmd_set_session_tag;
		var stmt = snowflake.createStatement({sqlText:next_stmt});
		stmt_result = stmt.execute();
		
		// 0 == false; needs to be done after refreshing the external table
		if (AUTO_COMMIT === 0){
			sql_cmd = "BEGIN;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		// iterate over execution array	
		for (i = 0; i < executions_array.length; i++){
			next_stmt = executions_array[i][0];
			stmt = snowflake.createStatement( {sqlText: next_stmt} );
			time_begin_sub = new Date().toISOString();
			stmt_result = stmt.execute();
			stmt_result.next();
			time_end_sub = new Date().toISOString();
			message = {
				"command":executions_array[i][1],
				"time_begin":time_begin_sub,
				"time_end":time_end_sub,
				"query_id":String(stmt_result.getQueryId())
			};
			messageArray.push(message);	
		};

		status = true;
		result.status = "succeeded";
		result.messages = messageArray;
		time_end_main = new Date().toISOString();
		result["time_end"] = time_end_main;
		
		// commit if auto commit = 0 == false
		if (AUTO_COMMIT === 0){
			sql_cmd = "COMMIT;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();;
		};
		
	} catch (err) {
		// if auto commit is disabled, it is required to rollback on error
		if (AUTO_COMMIT === 0){
			sql_cmd = "ROLLBACK;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		status = false;
		result.status = "failed";
		result.messages = err;
		result["time_end"] = new Date().toISOString();
		var res_string = JSON.stringify(result);
		
		error_message	= JSON.stringify(result, null, 2);	
		
		var sql_raise_error = "DECLARE customException EXCEPTION(-20001, '"+error_message+"'); "
							+ "RAISE customException;";
							
		stmt = snowflake.createStatement( {sqlText: sql_raise_error} );
		stmt_result = stmt.execute();
	}
	
	return result;	
$$
;