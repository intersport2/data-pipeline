
create or replace procedure INTERSPORT_DEV.SILVER.LOAD_ORDER_DETAILS_SP (
	AUTO_COMMIT boolean
)
returns variant
language javascript
execute as caller
as
$$
	
	var status;
	var result = {
		"status":"",
		"time_begin":"",
		"time_end":"",
		"messages":""
	};
	var message;
	var messageArray = [];
	
	// variables: sql execution
	var stmt;
	var stmt_result;
	var next_stmt;

	var time_begin_main;
	var time_end_main;
	var time_begin_sub;
	var time_end_sub;
	
	var error_message;
	
	// sql table names
	var source_table												= "BRONZE.RAW_ORDER"
	var target_table												= "SILVER.ORDER_DETAILS"
	var product_table												= "SILVER.PRODUCT_DATA"
	var customer_table												= "SILVER.CUSTOMER_DATA"
	var temp_order_insert_stage_table								= "ORDER_STAGE_INSERT_TBL"

	// variables: sql statements
	
	/*
	 Create temporary staging table for storing all unprocessed order data
	*/
	var sql_create_temp_order_insert_stage_table = `
				create or replace temporary table ${temp_order_insert_stage_table} as
				SELECT *
				FROM ${source_table} WHERE is_processed = FALSE 
				; 
	`;

	

	/*
	 Merge data into order details table
	*/
	var sql_cmd_merge_order_details = `
				merge into ${target_table} as tgt
				USING (SELECT 
				o.ORDER_ID,
				c.ID as CUSTOMER_ID,
				p.ID as PRODUCT_ID,
				o.ORDER_DATE,
				o.SALE_PRICE,
				o.SALE_PERCENTAGE,
				o.COUPON_VALUE,
				o.ORDER_STATUS,
				o.NUMBER_ARTICLES as QUANTITY
				FROM ${temp_order_insert_stage_table} as o
				join ${product_table} as p
				on o.PRODUCT_ID = p.PRODUCT_ID 
				and o.ORDER_DATE BETWEEN p.EFFECTIVE_FROM AND IFNULL(p.EFFECTIVE_TILL, '2099-12-31')
				join ${customer_table} as c
				on o.CUSTOMER_ID = c.CUSTOMER_ID
				and o.ORDER_DATE BETWEEN c.EFFECTIVE_FROM AND IFNULL(c.EFFECTIVE_TILL, '2099-12-31')) as src
				on tgt.ORDER_ID = src.ORDER_ID
				WHEN MATCHED AND (tgt.CUSTOMER_ID <> src.CUSTOMER_ID OR
				tgt.PRODUCT_ID <> src.PRODUCT_ID OR
				tgt.ORDER_DATE <> src.ORDER_DATE OR
				tgt.SALE_PRICE <> src.SALE_PRICE OR
				tgt.SALE_PERCENTAGE <> src.SALE_PERCENTAGE OR
				tgt.COUPON_VALUE <> src.COUPON_VALUE OR
				tgt.ORDER_STATUS <> src.ORDER_STATUS OR
				tgt.QUANTITY <> src.QUANTITY) THEN UPDATE SET
				tgt.CUSTOMER_ID = src.CUSTOMER_ID,
				tgt.PRODUCT_ID = src.PRODUCT_ID,
				tgt.ORDER_DATE = src.ORDER_DATE,
				tgt.SALE_PRICE = src.SALE_PRICE,
				tgt.SALE_PERCENTAGE = src.SALE_PERCENTAGE,
				tgt.COUPON_VALUE = src.COUPON_VALUE,
				tgt.ORDER_STATUS = src.ORDER_STATUS,
				tgt.QUANTITY = src.QUANTITY,
				tgt.UPDATED_ON = CURRENT_TIMESTAMP()
				WHEN NOT MATCHED THEN INSERT (
				ORDER_ID,
				CUSTOMER_ID,
				PRODUCT_ID,
				ORDER_DATE,
				SALE_PRICE,
				SALE_PERCENTAGE,
				COUPON_VALUE,
				ORDER_STATUS,
				QUANTITY) values (
				src.ORDER_ID,
				src.CUSTOMER_ID,
				src.PRODUCT_ID,
				src.ORDER_DATE,
				src.SALE_PRICE,
				src.SALE_PERCENTAGE,
				src.COUPON_VALUE,
				src.ORDER_STATUS,
				src.QUANTITY)			
				;			
	`;

	/*
	 Update IS_PROCESSED flag in raw table
	*/
	var sql_cmd_update_flag_raw_table = `
				UPDATE ${source_table} SET IS_PROCESSED = TRUE 
				WHERE ID IN (SELECT ID FROM ${temp_order_insert_stage_table})				
				;			
	`;

	/*
	 Drop insert stage table
	*/
	var sql_cmd_drop_insert_stage_table = `
				DROP TABLE ${temp_order_insert_stage_table}				
				;			
	`;
	

	// list of sql commands to be executed and corresponding log message
	var executions_array = [];
	executions_array.push(
		[sql_create_temp_order_insert_stage_table, 'create temporary stage table '+temp_order_insert_stage_table],
		[sql_cmd_merge_order_details, 'merge records in to '+target_table],
		[sql_cmd_update_flag_raw_table, 'update IS_PROCESSED flag in '+source_table],
		[sql_cmd_drop_insert_stage_table, 'drop stage table '+temp_order_insert_stage_table]
		)

	try {
		time_begin_main = new Date().toISOString();
		result["time_begin"] = time_begin_main;
		
		// 0 == false; needs to be done after refreshing the external table
		if (AUTO_COMMIT === 0){
			sql_cmd = "BEGIN;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		// iterate over execution array	
		for (i = 0; i < executions_array.length; i++){
			next_stmt = executions_array[i][0];
			stmt = snowflake.createStatement( {sqlText: next_stmt} );
			time_begin_sub = new Date().toISOString();
			stmt_result = stmt.execute();
			stmt_result.next();
			time_end_sub = new Date().toISOString();
			message = {
				"command":executions_array[i][1],
				"time_begin":time_begin_sub,
				"time_end":time_end_sub,
				"query_id":String(stmt_result.getQueryId())
			};
			messageArray.push(message);	
		};

		status = true;
		result.status = "succeeded";
		result.messages = messageArray;
		time_end_main = new Date().toISOString();
		result["time_end"] = time_end_main;
		
		// commit if auto commit = 0 == false
		if (AUTO_COMMIT === 0){
			sql_cmd = "COMMIT;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();;
		};
		
	} catch (err) {
		// if auto commit is disabled, it is required to rollback on error
		if (AUTO_COMMIT === 0){
			sql_cmd = "ROLLBACK;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		status = false;
		result.status = "failed";
		result.messages = err;
		result["time_end"] = new Date().toISOString();
		var res_string = JSON.stringify(result);
		
		error_message	= JSON.stringify(result, null, 2);	
		
		var sql_raise_error = "DECLARE customException EXCEPTION(-20001, '"+error_message+"'); "
							+ "RAISE customException;";
							
		stmt = snowflake.createStatement( {sqlText: sql_raise_error} );
		stmt_result = stmt.execute();
	}
	
	return result;	
$$
;