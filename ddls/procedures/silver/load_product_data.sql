
create or replace procedure INTERSPORT_DEV.SILVER.LOAD_PRODUCT_DATA_SP (
	AUTO_COMMIT boolean
)
returns variant
language javascript
execute as caller
as
$$
	
	var status;
	var result = {
		"status":"",
		"time_begin":"",
		"time_end":"",
		"messages":""
	};
	var message;
	var messageArray = [];
	
	// variables: sql execution
	var stmt;
	var stmt_result;
	var next_stmt;

	var time_begin_main;
	var time_end_main;
	var time_begin_sub;
	var time_end_sub;
	
	var error_message;
	
	// sql table names
	var source_table												= "BRONZE.RAW_PRODUCT"
	var target_table												= "SILVER.PRODUCT_DATA"
	var currency_table												= "SILVER.EXCHANGE_RATES"
	var vertical_table												= "SILVER.PRODUCT_VERTICAL"
	var sub_vertical_table											= "SILVER.PRODUCT_SUB_VERTICAL"
	var temp_product_insert_stage_table								= "PRODUCT_STAGE_INSERT_TBL";
	var temp_product_update_stage_table 							= "PRODUCT_STAGE_UPDATE_TBL";

	// variables: sql statements
	
	/*
	 Create temporary staging table for storing all unprocessed product data
	*/
	var sql_create_temp_product_insert_stage_table = `
				create or replace temporary table ${temp_product_insert_stage_table} as
				SELECT *
				FROM ${source_table} WHERE is_processed = FALSE 
				; 
	`;

	/*
	 Create temporary staging table for product data which has to be updated
	*/
	var sql_create_temp_product_update_stage_table = `
				create or replace temporary table ${temp_product_update_stage_table} as
				SELECT *
				FROM ${source_table} WHERE is_processed = FALSE AND
				PRODUCT_ID IN (SELECT PRODUCT_ID FROM ${target_table})
				; 
	`;
			
	/*
	 Update effective_till date for SCD type 2
	*/
	var sql_update_historic_data = `
				update ${target_table} 
				SET EFFECTIVE_TILL = CURRENT_TIMESTAMP()
				WHERE PRODUCT_ID IN (SELECT PRODUCT_ID FROM ${temp_product_update_stage_table})
				AND EFFECTIVE_TILL IS NULL					
				;			
	`;

	/*
	 Insert the data into product_data
	*/
	var sql_cmd_insert_product_data = `
				insert into ${target_table} 
				(PRODUCT_ID,
				PRODUCT_NAME,
				BRAND,
				RRP,
				CURRENCY_ID,
				SUB_VERTICAL_ID,
				EFFECTIVE_FROM)
				SELECT PRODUCT_ID,
				PRODUCT_NAME,
				BRAND,
				RRP,
				er.EX_ID as CURRENCY_ID,
				sv.SUB_VERTICAL_ID,
				CURRENT_TIMESTAMP() AS EFFECTIVE_FROM
				from
				 ${temp_product_insert_stage_table} as pr
				 join ${vertical_table} v
				 on pr.PRODUCT_MAIN_CATEGORY = v.vertical
				 join ${sub_vertical_table} sv
				 on v.vertical_id = sv.vertical_id and pr.PRODUCT_CATEGORY = sv.SUB_VERTICAL
				 join ${currency_table} er
				 on er.CURRENCY = pr.CURRENCY and
				 pr.ADDED_ON BETWEEN er.EFFECTIVE_FROM AND IFNULL(er.EFFECTIVE_TILL, '2099-12-31')			
				;			
	`;

	/*
	 Update IS_PROCESSED flag in raw table
	*/
	var sql_cmd_update_flag_raw_table = `
				UPDATE ${source_table} SET IS_PROCESSED = TRUE 
				WHERE ID IN (SELECT ID FROM ${temp_product_insert_stage_table})				
				;			
	`;

	/*
	 Drop insert stage table
	*/
	var sql_cmd_drop_insert_stage_table = `
				DROP TABLE ${temp_product_insert_stage_table}				
				;			
	`;

	/*
	 Update update stage table
	*/
	var sql_cmd_drop_update_stage_table = `
				DROP TABLE ${temp_product_update_stage_table}				
				;			
	`;
	

	// list of sql commands to be executed and corresponding log message
	var executions_array = [];
	executions_array.push(
		[sql_create_temp_product_insert_stage_table, 'create temporary stage table '+temp_product_insert_stage_table],
		[sql_create_temp_product_update_stage_table, 'create temporary stage table '+temp_product_update_stage_table],
		[sql_update_historic_data, 'update historic data in '+target_table],
		[sql_cmd_insert_product_data, 'insert new records in '+target_table],
		[sql_cmd_update_flag_raw_table, 'update IS_PROCESSED flag in '+source_table],
		[sql_cmd_drop_insert_stage_table, 'drop stage table '+temp_product_insert_stage_table],
		[sql_cmd_drop_update_stage_table, 'drop stage table '+temp_product_update_stage_table]
		)

	try {
		time_begin_main = new Date().toISOString();
		result["time_begin"] = time_begin_main;
		
		// 0 == false; needs to be done after refreshing the external table
		if (AUTO_COMMIT === 0){
			sql_cmd = "BEGIN;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		// iterate over execution array	
		for (i = 0; i < executions_array.length; i++){
			next_stmt = executions_array[i][0];
			stmt = snowflake.createStatement( {sqlText: next_stmt} );
			time_begin_sub = new Date().toISOString();
			stmt_result = stmt.execute();
			stmt_result.next();
			time_end_sub = new Date().toISOString();
			message = {
				"command":executions_array[i][1],
				"time_begin":time_begin_sub,
				"time_end":time_end_sub,
				"query_id":String(stmt_result.getQueryId())
			};
			messageArray.push(message);	
		};

		status = true;
		result.status = "succeeded";
		result.messages = messageArray;
		time_end_main = new Date().toISOString();
		result["time_end"] = time_end_main;
		
		// commit if auto commit = 0 == false
		if (AUTO_COMMIT === 0){
			sql_cmd = "COMMIT;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();;
		};
		
	} catch (err) {
		// if auto commit is disabled, it is required to rollback on error
		if (AUTO_COMMIT === 0){
			sql_cmd = "ROLLBACK;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		status = false;
		result.status = "failed";
		result.messages = err;
		result["time_end"] = new Date().toISOString();
		var res_string = JSON.stringify(result);
		
		error_message	= JSON.stringify(result, null, 2);	
		
		var sql_raise_error = "DECLARE customException EXCEPTION(-20001, '"+error_message+"'); "
							+ "RAISE customException;";
							
		stmt = snowflake.createStatement( {sqlText: sql_raise_error} );
		stmt_result = stmt.execute();
	}
	
	return result;	
$$
;