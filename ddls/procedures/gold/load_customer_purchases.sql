
create or replace procedure INTERSPORT_DEV.GOLD.LOAD_CUSTOMER_PURCHASES_SP (
	AUTO_COMMIT boolean
)
returns variant
language javascript
execute as caller
as
$$
	
	var status;
	var result = {
		"status":"",
		"time_begin":"",
		"time_end":"",
		"messages":""
	};
	var message;
	var messageArray = [];
	
	// variables: sql execution
	var stmt;
	var stmt_result;
	var next_stmt;

	var time_begin_main;
	var time_end_main;
	var time_begin_sub;
	var time_end_sub;
	
	var error_message;
	
	// sql table names
	var customer_table												= "SILVER.CUSTOMER_DATA"
	var order_table													= "SILVER.ORDER_DETAILS"
	var product_table												= "SILVER.PRODUCT_DATA"
	var currency_table												= "SILVER.EXCHANGE_RATES"
	var target_table												= "GOLD.CUSTOMER_PURCHASES"
	var temp_table													= "STAGE_TBL"

	// variables: sql statements
	
	/*
	 Create temporary staging table
	*/
	var sql_create_temp_table = `
				create or replace temporary table ${temp_table} as
				SELECT 
				c.ID,
                c.CUSTOMER_ID,
				c.FIRST_NAME,
				c.LAST_NAME,
				c.EMAIL_ADDRESS,
				c.COUNTRY,
                c.STREET,
                c.STREET_NUMBER,
                c.ZIP_CODE,
				er.CURRENCY,
				o.ORDER_STATUS,
				SUM(o.SALE_PRICE * o.QUANTITY) AS TOTAL_PURCHASE_AMOUNT,
				SUM(o.SALE_PRICE * o.QUANTITY * er.rate) AS TOTAL_PURCHASE_AMOUNT_IN_EUR,
				COUNT(o.ORDER_ID) AS TOTAL_PURCHASES,
				MAX(o.ORDER_DATE) as LAST_PURCHASE_DATE
				FROM ${customer_table} as c
				JOIN ${order_table} as o
				ON c.ID = o.CUSTOMER_ID
				AND o.ADDED_ON >= (SELECT MAX(ADDED_ON) FROM ${target_table})
				JOIN ${product_table} as p
				ON p.ID = o.PRODUCT_ID
				JOIN ${currency_table} as er
				ON er.EX_ID = p.CURRENCY_ID
				AND o.ORDER_DATE BETWEEN er.EFFECTIVE_FROM AND IFNULL(er.EFFECTIVE_TILL, '2099-12-31')
				GROUP BY c.ID,
                c.CUSTOMER_ID,
				c.FIRST_NAME,
				c.LAST_NAME,
				c.EMAIL_ADDRESS,
				c.COUNTRY,
                c.STREET,
                c.STREET_NUMBER,
                c.ZIP_CODE,
				er.CURRENCY,
				o.ORDER_STATUS
				; 
	`;

	

	/*
	 Merge data into customer_purchases table
	*/
	var sql_cmd_merge_customer_purchases = `
				merge into ${target_table} as tgt
				USING ${temp_table} as src
				on (tgt.ID = src.ID AND
				tgt.CURRENCY = src.CURRENCY AND
				tgt.ORDER_STATUS = src.ORDER_STATUS)
				WHEN MATCHED AND (tgt.CUSTOMER_ID <> src.CUSTOMER_ID OR
				tgt.FIRST_NAME <> src.FIRST_NAME OR
				tgt.LAST_NAME <> src.LAST_NAME OR
				tgt.EMAIL_ADDRESS <> src.EMAIL_ADDRESS OR
				tgt.COUNTRY <> src.COUNTRY OR
				tgt.STREET <> src.STREET OR
				tgt.STREET_NUMBER <> src.STREET_NUMBER OR
				tgt.ZIP_CODE <> src.ZIP_CODE OR
				tgt.TOTAL_PURCHASE_AMOUNT <> src.TOTAL_PURCHASE_AMOUNT OR
				tgt.TOTAL_PURCHASE_AMOUNT_IN_EUR <> src.TOTAL_PURCHASE_AMOUNT_IN_EUR OR
				tgt.TOTAL_PURCHASES <> src.TOTAL_PURCHASES OR
				tgt.LAST_PURCHASE_DATE <> src.LAST_PURCHASE_DATE) THEN UPDATE SET
				tgt.CUSTOMER_ID = src.CUSTOMER_ID,
				tgt.FIRST_NAME = src.FIRST_NAME,
				tgt.LAST_NAME = src.LAST_NAME,
				tgt.EMAIL_ADDRESS = src.EMAIL_ADDRESS,
				tgt.COUNTRY = src.COUNTRY,
				tgt.STREET = src.STREET,
				tgt.STREET_NUMBER = src.STREET_NUMBER,
				tgt.ZIP_CODE = src.ZIP_CODE,
				tgt.TOTAL_PURCHASE_AMOUNT = src.TOTAL_PURCHASE_AMOUNT,
				tgt.TOTAL_PURCHASE_AMOUNT_IN_EUR = src.TOTAL_PURCHASE_AMOUNT_IN_EUR,
				tgt.TOTAL_PURCHASES = src.TOTAL_PURCHASES,
				tgt.LAST_PURCHASE_DATE = src.LAST_PURCHASE_DATE,
				tgt.UPDATED_ON = CURRENT_TIMESTAMP()
				WHEN NOT MATCHED THEN INSERT (
				ID,
				CUSTOMER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL_ADDRESS,
				COUNTRY,
				STREET,
				STREET_NUMBER,
				ZIP_CODE,
				CURRENCY,
				ORDER_STATUS,
				TOTAL_PURCHASE_AMOUNT,
				TOTAL_PURCHASE_AMOUNT_IN_EUR,
				TOTAL_PURCHASES,
				LAST_PURCHASE_DATE) values (
				src.ID,
				src.CUSTOMER_ID,
				src.FIRST_NAME,
				src.LAST_NAME,
				src.EMAIL_ADDRESS,
				src.COUNTRY,
				src.STREET,
				src.STREET_NUMBER,
				src.ZIP_CODE,
				src.CURRENCY,
				src.ORDER_STATUS,
				src.TOTAL_PURCHASE_AMOUNT,
				src.TOTAL_PURCHASE_AMOUNT_IN_EUR,
				src.TOTAL_PURCHASES,
				src.LAST_PURCHASE_DATE)			
				;			
	`;

	/*
	 Drop temp stage table
	*/
	var sql_cmd_drop_temp_table = `
				DROP TABLE ${temp_table}				
				;			
	`;
	

	// list of sql commands to be executed and corresponding log message
	var executions_array = [];
	executions_array.push(
		[sql_create_temp_table, 'create temporary stage table '+temp_table],
		[sql_cmd_merge_customer_purchases, 'merge records in to '+target_table],
		[sql_cmd_drop_temp_table, 'drop temp table '+temp_table]
		)

	try {
		time_begin_main = new Date().toISOString();
		result["time_begin"] = time_begin_main;
		
		// 0 == false; needs to be done after refreshing the external table
		if (AUTO_COMMIT === 0){
			sql_cmd = "BEGIN;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		// iterate over execution array	
		for (i = 0; i < executions_array.length; i++){
			next_stmt = executions_array[i][0];
			stmt = snowflake.createStatement( {sqlText: next_stmt} );
			time_begin_sub = new Date().toISOString();
			stmt_result = stmt.execute();
			stmt_result.next();
			time_end_sub = new Date().toISOString();
			message = {
				"command":executions_array[i][1],
				"time_begin":time_begin_sub,
				"time_end":time_end_sub,
				"query_id":String(stmt_result.getQueryId())
			};
			messageArray.push(message);	
		};

		status = true;
		result.status = "succeeded";
		result.messages = messageArray;
		time_end_main = new Date().toISOString();
		result["time_end"] = time_end_main;
		
		// commit if auto commit = 0 == false
		if (AUTO_COMMIT === 0){
			sql_cmd = "COMMIT;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();;
		};
		
	} catch (err) {
		// if auto commit is disabled, it is required to rollback on error
		if (AUTO_COMMIT === 0){
			sql_cmd = "ROLLBACK;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		status = false;
		result.status = "failed";
		result.messages = err;
		result["time_end"] = new Date().toISOString();
		var res_string = JSON.stringify(result);
		
		error_message	= JSON.stringify(result, null, 2);	
		
		var sql_raise_error = "DECLARE customException EXCEPTION(-20001, '"+error_message+"'); "
							+ "RAISE customException;";
							
		stmt = snowflake.createStatement( {sqlText: sql_raise_error} );
		stmt_result = stmt.execute();
	}
	
	return result;	
$$
;