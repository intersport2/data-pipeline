
create or replace procedure INTERSPORT_DEV.GOLD.LOAD_PRODUCT_PURCHASES_SP (
	AUTO_COMMIT boolean
)
returns variant
language javascript
execute as caller
as
$$
	
	var status;
	var result = {
		"status":"",
		"time_begin":"",
		"time_end":"",
		"messages":""
	};
	var message;
	var messageArray = [];
	
	// variables: sql execution
	var stmt;
	var stmt_result;
	var next_stmt;

	var time_begin_main;
	var time_end_main;
	var time_begin_sub;
	var time_end_sub;
	
	var error_message;
	
	// sql table names
	var order_table													= "SILVER.ORDER_DETAILS"
	var product_table												= "SILVER.PRODUCT_DATA"
	var currency_table												= "SILVER.EXCHANGE_RATES"
	var target_table												= "GOLD.PRODUCT_PURCHASES"
	var temp_table													= "STAGE_TBL"

	// variables: sql statements
	
	/*
	 Create temporary staging table
	*/
	var sql_create_temp_table = `
				create or replace temporary table ${temp_table} as
				SELECT 
				p.ID,
                p.PRODUCT_ID,
                o.ORDER_DATE::DATE AS PURCHASE_DATE,
                p.PRODUCT_NAME,
                p.BRAND,
                COUNT(o.ORDER_ID) AS TOTAL_PURCHASES,
                er.CURRENCY,
				o.ORDER_STATUS,
                SUM(o.SALE_PRICE * o.QUANTITY) AS TOTAL_SALES_AMOUNT,
                SUM(o.SALE_PRICE * o.QUANTITY * er.RATE) AS TOTAL_SALES_AMOUNT_IN_EUR
				FROM ${product_table} as p
				JOIN ${order_table} as o
				ON p.ID = o.PRODUCT_ID
				AND o.ADDED_ON >= (SELECT MAX(ADDED_ON) FROM ${target_table})
				JOIN ${currency_table} as er
				ON er.EX_ID = p.CURRENCY_ID
				AND o.ORDER_DATE BETWEEN er.EFFECTIVE_FROM AND IFNULL(er.EFFECTIVE_TILL, '2099-12-31')
				GROUP BY p.ID,
                p.PRODUCT_ID,
                o.ORDER_DATE::DATE,
                p.PRODUCT_NAME,
                p.BRAND,
                er.CURRENCY,
				o.ORDER_STATUS
				; 
	`;

	

	/*
	 Merge data into product_purchases table
	*/
	var sql_cmd_merge_product_purchases = `
				merge into ${target_table} as tgt
				USING ${temp_table} as src
				on (tgt.ID = src.ID AND
				tgt.CURRENCY = src.CURRENCY AND
				tgt.ORDER_STATUS = src.ORDER_STATUS AND
				tgt.PURCHASE_DATE = src.PURCHASE_DATE)
				WHEN MATCHED AND (tgt.PRODUCT_ID <> src.PRODUCT_ID OR
				tgt.PRODUCT_NAME <> src.PRODUCT_NAME OR
				tgt.BRAND <> src.BRAND OR
				tgt.TOTAL_SALES_AMOUNT <> src.TOTAL_SALES_AMOUNT OR
				tgt.TOTAL_SALES_AMOUNT_IN_EUR <> src.TOTAL_SALES_AMOUNT_IN_EUR OR
				tgt.TOTAL_PURCHASES <> src.TOTAL_PURCHASES) THEN UPDATE SET
				tgt.PRODUCT_ID = src.PRODUCT_ID,
				tgt.PRODUCT_NAME = src.PRODUCT_NAME,
				tgt.BRAND = src.BRAND,
				tgt.TOTAL_SALES_AMOUNT = src.TOTAL_SALES_AMOUNT,
				tgt.TOTAL_SALES_AMOUNT_IN_EUR = src.TOTAL_SALES_AMOUNT_IN_EUR,
				tgt.TOTAL_PURCHASES = src.TOTAL_PURCHASES,
				tgt.UPDATED_ON = CURRENT_TIMESTAMP()
				WHEN NOT MATCHED THEN INSERT (
				ID,
                PRODUCT_ID,
                PURCHASE_DATE,
                PRODUCT_NAME,
                BRAND,
                TOTAL_PURCHASES,
                CURRENCY,
				ORDER_STATUS,
                TOTAL_SALES_AMOUNT,
                TOTAL_SALES_AMOUNT_IN_EUR) values (
				src.ID,
				src.PRODUCT_ID,
                src.PURCHASE_DATE,
                src.PRODUCT_NAME,
                src.BRAND,
                src.TOTAL_PURCHASES,
                src.CURRENCY,
				src.ORDER_STATUS,
                src.TOTAL_SALES_AMOUNT,
                src.TOTAL_SALES_AMOUNT_IN_EUR)			
				;			
	`;

	/*
	 Drop temp stage table
	*/
	var sql_cmd_drop_temp_table = `
				DROP TABLE ${temp_table}				
				;			
	`;
	

	// list of sql commands to be executed and corresponding log message
	var executions_array = [];
	executions_array.push(
		[sql_create_temp_table, 'create temporary stage table '+temp_table],
		[sql_cmd_merge_product_purchases, 'merge records in to '+target_table],
		[sql_cmd_drop_temp_table, 'drop temp table '+temp_table]
		)

	try {
		time_begin_main = new Date().toISOString();
		result["time_begin"] = time_begin_main;
		
		// 0 == false; needs to be done after refreshing the external table
		if (AUTO_COMMIT === 0){
			sql_cmd = "BEGIN;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		// iterate over execution array	
		for (i = 0; i < executions_array.length; i++){
			next_stmt = executions_array[i][0];
			stmt = snowflake.createStatement( {sqlText: next_stmt} );
			time_begin_sub = new Date().toISOString();
			stmt_result = stmt.execute();
			stmt_result.next();
			time_end_sub = new Date().toISOString();
			message = {
				"command":executions_array[i][1],
				"time_begin":time_begin_sub,
				"time_end":time_end_sub,
				"query_id":String(stmt_result.getQueryId())
			};
			messageArray.push(message);	
		};

		status = true;
		result.status = "succeeded";
		result.messages = messageArray;
		time_end_main = new Date().toISOString();
		result["time_end"] = time_end_main;
		
		// commit if auto commit = 0 == false
		if (AUTO_COMMIT === 0){
			sql_cmd = "COMMIT;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();;
		};
		
	} catch (err) {
		// if auto commit is disabled, it is required to rollback on error
		if (AUTO_COMMIT === 0){
			sql_cmd = "ROLLBACK;"
			stmt_exec = snowflake.createStatement({sqlText:sql_cmd}).execute();
		};
		
		status = false;
		result.status = "failed";
		result.messages = err;
		result["time_end"] = new Date().toISOString();
		var res_string = JSON.stringify(result);
		
		error_message	= JSON.stringify(result, null, 2);	
		
		var sql_raise_error = "DECLARE customException EXCEPTION(-20001, '"+error_message+"'); "
							+ "RAISE customException;";
							
		stmt = snowflake.createStatement( {sqlText: sql_raise_error} );
		stmt_result = stmt.execute();
	}
	
	return result;	
$$
;