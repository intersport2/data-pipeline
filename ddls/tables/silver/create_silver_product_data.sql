create or replace TABLE SILVER.PRODUCT_DATA (
	ID NUMBER(38,0) autoincrement start 1 increment 1 noorder,
    PRODUCT_ID NUMBER(38,0),
    PRODUCT_NAME VARCHAR,
    BRAND VARCHAR,
    RRP NUMBER(10,2),
    CURRENCY_ID NUMBER(10,0),
    SUB_VERTICAL_ID NUMBER(38,0),
	ADDED_ON TIMESTAMP_LTZ(9) DEFAULT CURRENT_TIMESTAMP(),
    EFFECTIVE_FROM TIMESTAMP_LTZ(9),
    EFFECTIVE_TILL TIMESTAMP_LTZ(9)
);

